var gulp = require('gulp');
var fn = require('gulp-load-plugins')();
fn.del = require('del');
fn.encode = require('./lib/encode');

var Config = {
    INPUT: 'src/main.js',
    OUTPUT: './compiled.min.js'
};


// ----------------------------------------------
// CLEAN
// ----------------------------------------------
gulp.task('clean', function (cb) {
    fn.del(Config.OUTPUT, cb);
});

// ----------------------------------------------
// COMPRESS
// ----------------------------------------------
gulp.task('compress', ['clean'], function (cb) {

    return gulp.src(Config.INPUT)
        .pipe(fn.uglify('app.js', {
            mangle: false
        }))
        .pipe(fn.rename({
            extname: '.min.js'
        }))
        .pipe(fn.encode())
        .pipe(fn.insert.prepend('javascript:('))
        .pipe(fn.insert.append(')()'))
        .pipe(gulp.dest('./'));
});

// ----------------------------------------------
// DEFAULT
// ----------------------------------------------
gulp.task('default', ['compress']);
