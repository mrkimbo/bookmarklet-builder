var through = require('through2');

module.exports = function (method, options) {

    return through.obj(function (file, encoding, done) {
        var processed, buffer;

        if (file.contents instanceof Buffer) {
            processed = encodeURIComponent(file.contents.toString()),
                buffer = new Buffer(processed);

            file.contents = buffer;
        }

        this.push(file);
        return done();
    });

};
