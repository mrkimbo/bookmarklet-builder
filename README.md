# Bookmarklet Builder #

Bare bones project to compile clean javascript source into bookmarklet-executable code

### Install Dependencies ###

```$ npm install```

### Compilation ###

```$ gulp```